import java.sql.Time;
import java.util.Arrays;

public class ColorSort {

   // Kasutatud materjal: https://www.baeldung.com/java-counting-sort

   enum Color {red, green, blue};
   static int rCount = 0;
   static int gCount = 0;
   static int bCount = 0;


   public static void main (String[] param) {
      ColorSort.Color[] balls = null;
      balls = new ColorSort.Color [100000];
      int len = balls.length;
      rCount = len;
      gCount = 0;
      bCount = 0;
      for (int i=0; i < len; i++) {
         balls[i] = ColorSort.Color.red;
      }
      speedTest(balls);

   }

   public static void reorder (Color[] balls) {
      int arrayLength = balls.length;

      Color[] temp = balls.clone();

      int[] count = new int[3];

      Arrays.fill(count, 0);

      for (Color ball : balls) {
         count[ball.ordinal()] += 1;
      }

      for (int i = 1; i < count.length; i++) {
         count[i] += count[i - 1];
      }

      int current;
      for (int i = arrayLength - 1; i >= 0; i--) {

         current = temp[i].ordinal();

         count[current] -= 1;
         balls[count[current]] = temp[i];
      }

   }

   public static void speedTest(Color[] balls) {

      long time = System.currentTimeMillis();

      Color[] temp = balls.clone();

      long time2 = System.currentTimeMillis() - time;

      System.out.println(time2);

      Color[] output = new Color[balls.length];

      long time3 = System.currentTimeMillis();

      for (int i = 0; i < balls.length ; i++) {
         output[i] =  balls[i];
      }

      long time4 = System.currentTimeMillis() - time3;
      System.out.println(time4);

   }
}
